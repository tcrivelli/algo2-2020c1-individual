#include "Conjunto.h"

template<class T>
Conjunto<T>::Conjunto() : _raiz(NULL), cantNodos(0) {}

template<class T>
Conjunto<T>::~Conjunto() {
    destruir(_raiz);
}

template<class T>
void Conjunto<T>::destruir(Nodo *&n) {
    if (n != NULL) {
        destruir(n->izq);
        destruir(n->der);
        delete (n);
        n = NULL;
        cantNodos--;
    }
}

template<class T>
bool Conjunto<T>::pertenece(const T &clave) const {
    bool esta = false;
    Nodo *actual = _raiz;
    while (actual != NULL) {
        if (clave == actual->valor) {
            esta = true;
        }
        if (clave < actual->valor) {
            actual = actual->izq;
        } else {
            actual = actual->der;
        }
    }
    return esta;
}

template<class T>
void Conjunto<T>::insertarAux(const T &clave, Nodo *n) {
    if (clave < n->valor) {
        if (n->izq == NULL) {
            n->izq = new Nodo(clave, n);
        } else {
            insertarAux(clave, n->izq);
        }
    } else {
        if (n->der == NULL) {
            n->der = new Nodo(clave, n);
        } else {
            insertarAux(clave, n->der);
        }
    }
}

template<class T>
void Conjunto<T>::insertar(const T &clave) {
    if (pertenece(clave))
        return;
    if (_raiz == NULL) {
        _raiz = new Nodo(clave, NULL);
    } else {
        insertarAux(clave, _raiz);
    }
    cantNodos++;
}

template<class T>
typename Conjunto<T>::Nodo *Conjunto<T>::buscarNodo(const T &clave) {
    typename Conjunto<T>::Nodo *buscador = this->_raiz;
    while (buscador->valor != clave) {
        if (buscador->valor < clave) {
            buscador = buscador->der;
        } else {
            buscador = buscador->izq;
        }
    }
    return buscador;
}

template<class T>
bool Conjunto<T>::esHoja(Conjunto<T>::Nodo *&n) {
    bool nodoSinHijos = false;
    if (n->izq == NULL && n->der == NULL) {
        nodoSinHijos = true;
    }
    return nodoSinHijos;
}

template<class T>
bool Conjunto<T>::actualizaSiSoloHijoIzquierdo(Conjunto<T>::Nodo *&n) {
    bool nodoConUnSoloHijoIzquierdo = false;
    if (n->izq != NULL && n->der == NULL and n->padre != NULL) {
        if (n->padre->izq == n) {
            n->padre->izq = n->izq;
        } else {
            n->padre->der = n->izq;
        }
        n->izq->padre = n->padre;
        nodoConUnSoloHijoIzquierdo = true;
    }
    return nodoConUnSoloHijoIzquierdo;
}

template<class T>
bool Conjunto<T>::actualizaSiSoloHijoDerecho(Conjunto<T>::Nodo *&n) {
    bool nodoConUnSoloHijoDerecho = false;
    if (n->izq == NULL && n->der != NULL && n->padre != NULL) {
        if (n->padre->izq == n) {
            n->padre->izq = n->der;
        } else {
            n->padre->der = n->der;
        }
        n->der->padre = n->padre;
        nodoConUnSoloHijoDerecho = true;
    }
    return nodoConUnSoloHijoDerecho;
}

template<class T>
bool Conjunto<T>::tieneDosHijos(Conjunto<T>::Nodo *&n) {
    bool nodoConDosHijos = false;
    if (n->izq != NULL && n->der != NULL) {
        nodoConDosHijos = true;
    }
    return nodoConDosHijos;
}

template<class T>
typename Conjunto<T>::Nodo *Conjunto<T>::buscarMinimoDerecha(Conjunto<T>::Nodo *&n) {
    Nodo *buscadorMinimoDerecha = n->der;
    while (buscadorMinimoDerecha->izq != NULL) {
        buscadorMinimoDerecha = buscadorMinimoDerecha->izq;
    }
    return buscadorMinimoDerecha;
}

template<class T>
void Conjunto<T>::remover(const T &clave) {
    Nodo *aBorrar = buscarNodo(clave);
    bool borre = false;
    if (esHoja(aBorrar)) {
        if (aBorrar->padre == NULL) {
            _raiz = NULL;
        } else if (aBorrar->padre->der == aBorrar) {
            aBorrar->padre->der = nullptr;
        } else {
            aBorrar->padre->izq = nullptr;
        }
        delete (aBorrar);
    } else if (aBorrar->izq != NULL && aBorrar->der == NULL) {
        actualizaSiSoloHijoIzquierdo(aBorrar);
        if (aBorrar->padre != NULL) {
            delete (aBorrar);
        } else {
            _raiz = aBorrar->izq;
            _raiz->padre = NULL;
            delete (aBorrar);
        }
    } else if (aBorrar->izq == NULL && aBorrar->der != NULL) {
        actualizaSiSoloHijoDerecho(aBorrar);
        if (aBorrar->padre != NULL) {
            delete (aBorrar);
        } else {
            _raiz = aBorrar->der;
            _raiz->padre = NULL;
            delete (aBorrar);
        }
    } else if (tieneDosHijos(aBorrar)) {
        Nodo *minimoDerecha = buscarMinimoDerecha(aBorrar);
        if (minimoDerecha->der == NULL) {
            aBorrar->valor = minimoDerecha->valor;
            if (minimoDerecha->padre->izq == minimoDerecha) {
                minimoDerecha->padre->izq = NULL;
            } else {
                minimoDerecha->padre->der = NULL;
            }
            delete (minimoDerecha);
        } else {
            aBorrar->valor = minimoDerecha->valor;
            if (minimoDerecha->padre->izq == minimoDerecha) {
                minimoDerecha->padre->izq = minimoDerecha->der;
                minimoDerecha->der->padre = minimoDerecha->padre;
            } else {
                minimoDerecha->padre->der = minimoDerecha->der;
                minimoDerecha->der->padre = minimoDerecha->padre;
            }
            delete (minimoDerecha);
        }
    }
    cantNodos--;
}

template<class T>
const T &Conjunto<T>::siguiente(const T &clave) {
    if (_raiz->valor == clave) {
        Nodo *nodo = _raiz->der;
        while (nodo->izq != NULL) {
            nodo = nodo->izq;
        }
        return nodo->valor;
    } else {
        Nodo *padre = _raiz;
        if (clave < padre->valor) {
            Nodo *buscador = padre->izq;
            while (clave != buscador->valor) {
                if (clave < buscador->valor) {
                    padre = buscador;
                    buscador = buscador->izq;
                } else {
                    if (clave > buscador->valor) {
                        padre = buscador;
                        buscador = buscador->der;
                    }
                }
            }
            Nodo *tmp = buscador;
            if (tmp->der != NULL) {
                tmp = buscador->der;
                while (tmp->izq != NULL) {
                    tmp = tmp->izq;
                }
                return tmp->valor;
            } else {
                return padre->valor;
            }
        } else {
            // busco la clave en el subárbol derecho
            Nodo *buscador = padre->der;
            while (clave != buscador->valor) {
                if (clave < buscador->valor) {
                    padre = buscador;
                    buscador = buscador->izq;
                } else {
                    if (clave > buscador->valor) {
                        padre = buscador;
                        buscador = buscador->der;
                    }
                }
            }
            Nodo *aux = buscador;
            if (aux->der != NULL) {
                aux = buscador->der;
                while (aux->izq != NULL) {
                    aux = aux->izq;
                }
                return aux->valor;
            } else {
                return padre->valor;
            }
        }
    }
}

template<class T>
const T &Conjunto<T>::minimo() const {
    Nodo *actual = _raiz;
    while (actual->izq != NULL) {
        actual = actual->izq;
    }
    return actual->valor;
}

template<class T>
const T &Conjunto<T>::maximo() const {
    Nodo *actual = _raiz;
    while (actual->der != NULL) {
        actual = actual->der;
    }
    return actual->valor;
}

template<class T>
unsigned int Conjunto<T>::cardinal() const {
    return cantNodos;
}

template<class T>
void Conjunto<T>::mostrar(std::ostream &o) const {
    mostrar(_raiz, o);
    o << endl;
}

template<class T>
void Conjunto<T>::mostrar(const Nodo* n, std::ostream &o) const {
    if (n != NULL) {
        mostrar(n->izq, o);
        o << " " << n->valor << " ";
        mostrar(n->der, o);
    }
}

