#ifndef CONJUNTO_H_
#define CONJUNTO_H_

#include <assert.h>
#include <string>
#include <iostream>

using namespace std;

template <class T>
class Conjunto
{
    public:

        // Constructor. Genera un conjunto vacío.
        Conjunto();

        // Destructor. Debe dejar limpia la memoria.
        ~Conjunto();

        // Inserta un elemento en el conjunto. Si este ya existe,
        // el conjunto no se modifica.
        void insertar(const T&);

        // Decide si un elemento pertenece al conjunto o no.
        bool pertenece(const T&) const;

        // Borra un elemento del conjunto. Si este no existe,
        // el conjunto no se modifica.
        void remover(const T&);

        // Siguiente elemento al recibido por párametro, en orden.
        const T& siguiente(const T& clave);

        // Devuelve el mínimo elemento del conjunto según <.
        const T& minimo() const;

        // Devuelve el máximo elemento del conjunto según <.
        const T& maximo() const;

        // Devuelve la cantidad de elementos que tiene el conjunto.
        unsigned int cardinal() const;

        // Muestra el conjunto.
        void mostrar(std::ostream&) const;

        private:

        int cantNodos;

        struct Nodo
        {
            // El constructor, toma el elemento al que representa el nodo.
            Nodo(const T& v, Nodo* p) :
                    valor(v), izq(NULL), der(NULL), padre(p) {
            }
            // El elemento al que representa el nodo.
            T valor;
            // Puntero a la raíz del subárbol izquierdo.
            Nodo* izq;
            // Puntero a la raíz del subárbol derecho.
            Nodo* der;
            // Puntero al padre del nodo en el que estoy parado
            Nodo* padre;
        };

        // Puntero a la raíz de nuestro árbol.
        Nodo* _raiz;

        // función auxiliar que uso en el destructor
        void destruir(Nodo* &n);

        // función que me permite buiscar el nodo que tiene la clave a eliminar
        Nodo* buscarNodo(const T& clave);

        // Devuelve true si y sólo si el nodo es hoja, es decir, no tiene hijos
        bool esHoja(Nodo* &n);

        // Devuelve true si y sólo si el nodo n tiene solo un hijo izquierdo
        bool actualizaSiSoloHijoIzquierdo(Nodo *&n);

        // Devuelve true si y sólo si el nodo n tiene un solo hijo derecho
        bool actualizaSiSoloHijoDerecho(Nodo* &n);

        // Devuelve true si y sólo si el nodo n tiene dos hijos
        bool tieneDosHijos(Nodo* &n);

        // Devuelve el nodo con el mínimo valor del subárbol derecho
        Nodo* buscarMinimoDerecha(Nodo* &n);

        void insertarAux(const T& clave, Nodo* n);

        void mostrar(const Nodo* n, std::ostream &o) const;
};

template<class T>
std::ostream& operator<<(std::ostream& os, const Conjunto<T>& c) {
	 c.mostrar(os);
	 return os;
}

#include "Conjunto.hpp"

#endif
