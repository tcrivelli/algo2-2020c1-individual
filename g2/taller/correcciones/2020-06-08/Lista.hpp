#include "Lista.h"

template <typename T>
Lista<T>::Lista(): _longitud(0), _primero(NULL), _ultimo(NULL) {
}

template<class T>
Lista<T>::Nodo::Nodo(const T& elem)
        : valor(elem), _siguiente(NULL), _anterior(NULL) {
}

template <typename T>
Lista<T>::Lista(const Lista<T>& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

// este es el destructor

template <typename T>
Lista<T>::~Lista() {
    Nodo* temp = _primero;
    while(temp != NULL){
        temp = temp -> _siguiente;
        delete(_primero);
        _primero = temp;
    }
}

template <typename T>
Lista<T>& Lista<T>::operator=(const Lista<T>& aCopiar) {
    Nodo* iterador;
    iterador = _primero;

    while(iterador != _ultimo){
        Nodo* _siguiente = iterador->_siguiente;
        delete(iterador);
        iterador = _siguiente;
    }
    delete(_ultimo);

    _primero = NULL;
    _ultimo = NULL;
    _longitud = 0;

    iterador = aCopiar._primero;
    while(iterador != NULL){
        this->agregarAtras(iterador->valor);
        iterador = iterador->_siguiente;
    }
    return *this;
}

template <typename T>
void Lista<T>::eliminar(Nat i) {
    Nodo* tmp;
    tmp = _primero;
    while(i != 0){
        tmp = tmp->_siguiente;
        i--;
    }

    if(tmp != _primero && tmp != _ultimo){
        tmp->_anterior->_siguiente = tmp->_siguiente;
        tmp->_siguiente->_anterior = tmp->_anterior;
    } else {
        if(tmp == _primero){
            if(tmp->_siguiente != NULL){
                tmp->_siguiente->_anterior = NULL;
            }
            _primero = tmp->_siguiente;
        }
    }

    if(tmp == _ultimo){
        if(tmp->_anterior != NULL){
            tmp->_anterior->_siguiente = NULL;
        }
        _ultimo = tmp->_anterior;
    }
    _longitud--;
    delete tmp;
}

template <typename T>
void Lista<T>::agregarAdelante(const T& elem) {
    Nodo* tmp = new Nodo(elem);
    tmp->_anterior = NULL;
    tmp->_siguiente = NULL;

    if(_longitud == 0){
        _primero = tmp;
        _ultimo = tmp;
    } else {
        tmp->_siguiente = _primero;
        _primero->_anterior = tmp;
        _primero = tmp;
    }
    _longitud++;
}

template <typename T>
void Lista<T>::agregarAtras(const T& elem) {
    Nodo* tmp = new Nodo(elem);
    tmp->_anterior = NULL;
    tmp->_siguiente = NULL;

    if(_longitud == 0){
        _primero = tmp;
        _ultimo = tmp;
    } else {
        _ultimo->_siguiente = tmp;
        tmp->_anterior = _ultimo;
        _ultimo = tmp;
    }
    _longitud++;
}

template <class T>
void Lista<T>::agregarEnCalquierPosicion(const T& elem, Nat i) {
    // la idea es que el nodo aux va a terninar apuntando a la posición que me pasan por parámetro
    Nodo* aux;
    // quiero agregar el nodo tmp en la posición y el elemento que me pasan por parámetro
    Nodo* tmp;
    if(i == 0){
        agregarAdelante(elem);
    } else if(i == _longitud){
        agregarAtras(elem);
    } else if(i > 0 || i < _longitud){
        aux = _primero;
        while(aux != NULL){
            aux = _primero->_siguiente;
        }
        aux->_anterior->_siguiente = tmp;
        tmp->_siguiente = aux;
        aux->_anterior = tmp;
        tmp->_anterior = aux->_anterior;
        _longitud++;
    }
}

template <typename T>
int Lista<T>::longitud() const {
    return _longitud;
}

template <typename T>
const T& Lista<T>::iesimo(Nat i) const {
    Nodo* actual = _primero;
    while(i > 0){
        actual = actual->_siguiente;
        i--;
    }
    return actual->valor;
}

template <typename T>
T& Lista<T>::iesimo(Nat i) {
    Nodo* actual = _primero;
    while(i > 0){
        actual = actual->_siguiente;
        i--;
    }
    return actual->valor;
}

template <typename T>
void Lista<T>::mostrar(ostream& o) {
    Nodo *aux = _primero;
    cout << "[";
    while (aux != NULL && aux != _ultimo) {
        o << aux->valor << ",";
        aux = aux->_siguiente;
    }
    if (aux == _ultimo) {
        o << aux->valor << "]";
    }
}