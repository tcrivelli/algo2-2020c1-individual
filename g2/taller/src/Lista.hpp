#include "Lista.h"

Lista::Lista(): _longitud(0), _primero(NULL), _ultimo(NULL) {
}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {
    Nodo* temp = _primero;
    while(temp != NULL){
        temp = temp -> _siguiente;
        delete(_primero);
        _primero = temp;
    }
}

Lista& Lista::operator=(const Lista& aCopiar) {
    Nodo* iterador;
    iterador = _primero;

    while(iterador != _ultimo){
        Nodo* _siguiente = iterador->_siguiente;
        delete(iterador);
        iterador = _siguiente;
    }
    delete(_ultimo);

    _primero = NULL;
    _ultimo = NULL;
    _longitud = 0;

    iterador = aCopiar._primero;
    while(iterador != NULL){
        this->agregarAtras(iterador->valor);
        iterador = iterador->_siguiente;
    }
    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    Nodo* tmp = new Nodo(elem);
    tmp->_anterior = NULL;
    tmp->_siguiente = NULL;

    if(_longitud == 0){
        _primero = tmp;
        _ultimo = tmp;
    } else {
        tmp->_siguiente = _primero;
        _primero->_anterior = tmp;
        _primero = tmp;
    }
    _longitud++;
}

void Lista::agregarAtras(const int& elem) {
    Nodo* tmp = new Nodo(elem);
    tmp->_anterior = NULL;
    tmp->_siguiente = NULL;

    if(_longitud == 0){
        _primero = tmp;
        _ultimo = tmp;
    } else {
        _ultimo->_siguiente = tmp;
        tmp->_anterior = _ultimo;
        _ultimo = tmp;
    }
    _longitud++;
}

void Lista::eliminar(Nat i) {
    Nodo* tmp;
    tmp = _primero;
    while(i != 0){
        tmp = tmp->_siguiente;
        i--;
    }

    if(tmp != _primero && tmp != _ultimo){
        tmp->_anterior->_siguiente = tmp->_siguiente;
        tmp->_siguiente->_anterior = tmp->_anterior;
    } else {
        if(tmp == _primero){
            if(tmp->_siguiente != NULL){
                tmp->_siguiente->_anterior = NULL;
            }
            _primero = tmp->_siguiente;
        }
    }

    if(tmp == _ultimo){
        if(tmp->_anterior != NULL){
            tmp->_anterior->_siguiente = NULL;
        }
        _ultimo = tmp->_anterior;
    }
    _longitud--;
    delete tmp;
}

int Lista::longitud() const {
    return _longitud;
}

const int& Lista::iesimo(Nat i) const {
    Nodo* actual = _primero;
    while(i > 0){
        actual = actual->_siguiente;
        i--;
    }
    return actual->valor;
}

int& Lista::iesimo(Nat i) {
    Nodo* actual = _primero;
    while(i > 0){
        actual = actual->_siguiente;
        i--;
    }
    return actual->valor;
}

void Lista::mostrar(ostream& o) {
    Nodo *aux = _primero;
    cout << "[";
    while (aux != NULL && aux != _ultimo) {
        o << aux->valor << ",";
        aux = aux->_siguiente;
    }
    if (aux == _ultimo) {
        o << aux->valor << "]";
    }
}
